from django.contrib import admin

from .models import Object , Tag , Specifications
# from .models import ContactUs
# Register your models here.


# class SpecificationsInline(admin.TabularInline):
#     model = Object.specifications.through
#
# class TagInline(admin.TabularInline):
#     model = Object.tag.through

@admin.register(Object)
class ObjectAdmin(admin.ModelAdmin):
    list_display = ('name' , 'company' , 'category')
    list_filter = ('category' , 'company')
    search_fields = ('name' , 'company' , 'category')

    # inlines = [TagInline]
    # inlines = [SpecificationsInline]


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('name',)
    # pass

@admin.register(Specifications)
class Specifications(admin.ModelAdmin):
    list_display = ('key' , 'value')


